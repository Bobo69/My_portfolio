
import { ProjectService } from "../Service/projectService";
import { HomeView } from "../View/home";
import $ from "jquery";



export class HomeController {
  constructor(){
    let repo = new ProjectService();                                           //ont initialise notre repo class, le MODEL
    let promise = repo.findAll();                                                     //recupere tt les products list

    promise.then(response => {                                                        // ont traite la promise
      //console.log(response);                                                            /*[0] instanceof ProductList*/
      new HomeView(response);                                                         //ont instancie une new vue
    });
    $(document).on("deleteProjectList",(event, project) => {
      // console.log(data);
      repo.delete(project.id).then(() => {
        repo.findAll().then(response => {
          new HomeView(response);
        });
      });
    });
    
    $(document).on("addProjectList",(event, project) => {
      console.log(project);
      repo.add(project).then(() => {
        repo.findAll().then(response => {
          new HomeView(response);
        });
      });
    });
  }
}