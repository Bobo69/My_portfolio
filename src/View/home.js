"use strict";

import $ from "jquery";
import { Project } from "../Entity/project";

//le view manipule le DOM
export class HomeView {
  constructor(list = []) {
    $("body").empty();
    let ul = document.createElement("ul");
    let form = document.createElement("form");
    let input = document.createElement("input");
    let submit = document.createElement("input");

    submit.setAttribute("type", "submit");
    submit.setAttribute("value", "Ok");
    $(form).append(input).append(submit);

    $(form).submit(event => {                                                          
      event.preventDefault();
      let project = new Project(null, input.value);
      $(document).trigger("addProject", project);
      //console.log(input.value);
    });
    
    list.forEach(project => {
      let li = document.createElement("li");
      let span = document.createElement("span");
      let button = document.createElement("button");

      button.textContent = 'supprimer';
      $(button).click(() => {
          $(document).trigger("deleteproject", project);                        //event personnaliser ce n'est pas un event standard de HTML
      });

      span.textContent = project.name;

      $(li).append(span).append(button);
      $(ul).append(li);                                                                 //(ul)permet d'acceder a une methode de jquery, ("ul")est un query selector
    });

    $("body").append(ul);

    $("body").append(form);
  }
}