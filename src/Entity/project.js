"use strict";

export class Project {
  constructor(title, description, image, url, urlGit) {
      this.title = title;
      this.description = description;
      this.image = image;
      this.url = url;
      this.urlGit = urlGit
    }
}
