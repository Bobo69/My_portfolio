"use strict";
import $ from 'jquery';
import { Project } from '../Entity/project';

/**
 * Cette classe permettra de regrouper toutes les méthodes d'accès
 * à l'API REST via des requêtes AJAX.
 * L'idée est de ne pas avoir des appels AJAX brut qui traînent dans
 * l'appli et de n'utiliser que cette classe pour ça (elle fera
 * le CRUD en HTTP en gros)
 */
export class ProjectService {
    constructor() {
        this.baseUrl = "http://localhost:8080/project";
    }
    findAll() {
        let url = this.baseUrl;
        let promise = $.ajax(url).then(function(response) {
            let projectLists = [];
            response = JSON.parse(response);

            for (const item of response) {
                let instance = new Project(item.title, item.description, item.image, item.url, item.urlGit);
                projectLists.push(instance);
            }
            return projectLists;
        });
        
        promise.catch(error => {
            console.error(`${error.status} ${error.statusText} ${url}`);
        });
        return promise;
    }

    delete(id){
        let url = `${this.baseUrl}/${id}`;
        let promise = $.ajax(url, {method: "DELETE"}); 
        
        promise.catch(error => {
            console.error(`${error.status} ${error.statusText} ${url}`);
        });
        return promise;
    }

    add(project){
        let url = this.baseUrl;
        let json = JSON.stringify(project);
        let promise = $.ajax(url, {method: "POST", data: json});
        
        promise.catch(error => {
            console.error(`${error.status} ${error.statusText} ${url}`);
        });
        return promise;
    }


    // findAll() {
    //     /**
    //      * On fait un return de $.ajax pour passer la Promise lorsqu'on
    //      * appelle cette méthode (il faudra donc faire un .then() 
    //      * quand on l'appelera)
    //      */
    //     return $.ajax('http://localhost:8080/project')
    //     /* On met un .then() ici pour transformer les données de
    //     la promesses afin que lorsqu'on appelle cette méthode on
    //     manipule directement une Promise de Product[] plutôt que
    //     une Promise d'Object ressemblant vaguement à un Product */
    //     .then(function(data) {
    //         let tab = [];
    //         //On boucle sur les données brutes
    //         for (const item of data) {
    //             //On crée une instance de Product pour chaque objet
    //             let project = new Project(item.title, item.description, item.image, item.url, item.urlGit);
    //             tab.push(project);
    //         }
    //         //on return le tableau de Product pour le .then() suivant
    //         return tab;
    //     });
    // }
}
